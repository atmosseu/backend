import uuid
from datetime import datetime, timedelta
import pymysql.cursors
import os
import json
from User import User
from Session import Session

with open("sql_config.json") as db_config:
        DB_INFO = json.load(db_config)['mysql']

def get_connection():
    return pymysql.connect(host=DB_INFO['host'], port=DB_INFO['port'], user=DB_INFO['user'], password=DB_INFO['password'], db=DB_INFO['db'], charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)

class sessionUtils:
	def generate_session_id():
		return uuid.uuid4().hex

	def add_session_to_db(session):
		connection = get_connection()
		
		with connection.cursor() as cursor:
			sql = "INSERT INTO sessions (sess_id, expires, user_id, user_type, user_ip, last_activity) VALUES (%s, %s, %s, %s, %s, %s)"
			cursor.execute(sql, [session.get_sess_id(), session.get_expires(), session.get_user_id(), session.get_user_type(), session.get_user_ip(), session.get_last_activity()])
			connection.commit()
			connection.close()
		return True

	def does_session_exist(session_id):
		connection = get_connection()

		with connection.cursor() as cursor:
			sql = "SELECT * from sessions where sess_id= %s"
			cursor.execute(sql, session_id)
			result = cursor.fetchone()
			connection.close()
		if result:
			return True
		return False

	def get_session(session_id):
		connection = get_connection()

		with connection.cursor() as cursor:
			sql = "SELECT * from sessions where sess_id = %s"
			cursor.execute(sql, session_id)
			result = cursor.fetchone()
			connection.close()
			
		if result:
			session = Session(result['user_id'], result['sess_id'], result['expires'], result['user_type'], result['user_ip'], result['last_activity']) 
		else:
			session = False
		return session

	def open_session(sess_id, user, ip):
		current_time = datetime.utcnow()
		latest_activity = current_time.strftime('%Y-%m-%d %H:%M:%S')
		expires = current_time + timedelta(hours=12)
		expires = expires.strftime('%Y-%m-%d %H:%M:%S')
		session = Session(user.get_user_id(), sess_id, expires, user.get_user_type(), ip, latest_activity)
		return session


	def verify_session_ip(session_id, user_ip):
		if does_session_exist(session_id):
			session = get_session(session_id)
			if session.get_user_ip == user_ip:
				return True
		return False

	def check_session_expired(session_id):
		connection = get_connection()

		with connection.cursor() as cursor:
			sql = "SELECT expires from sessions where sess_id = %s"
			cursor.execute(sql, session_id)
			result = cursor.fetchone()
			connection.close()

		current_time = datetime.utcnow()
		session_expiry = datetime.strptime(result,'%Y-%m-%d %H:%M:%S')
		if current_time > session_expiry:
			return True
		return False

	def kill_session(session_id):
		connection = get_connection()

		with connection.cursor() as cursor:
			sql = "DELETE FROM sessions where sess_id=%s"
			cursor.execute(sql, session_id)
			connection.commit()
			connection.close()
		return True


	def update_session(session_id):
		current_time = datetime.utcnow()
		latest_activity = current_time.strftime('%Y-%m-%d %H:%M:%S')
		expires = current_time + timedelta(hours=12)
		expires = expires.strftime('%Y-%m-%d %H:%M:%S')
		connection = get_connection()

		with connection.cursor() as cursor:
			sql = "UPDATE sessions SET expires=%s, latest_activity=%s where sess_id=%s"
			cursor.execute(sql, [expires, latest_activity, session_id])
			connection.commit()
			connection.close()
		return True



if __name__ == "__main__":
    main()