import http.server
import http.cookies
from http.server import HTTPServer, BaseHTTPRequestHandler
import json
import urllib.parse
import requests
from loginUtils import loginUtils
from sessionUtils import sessionUtils
from User import User
from Session import Session
from datetime import datetime

# Class Description: Acts in the backend of the Demand side server. Handles HTTP Requests sent to the server.
class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

	# handle POST request
	def do_POST(self):
		
		# get path of the request and then determine where the request was sent to
		path = self.path
		endpoint = path.split("/")[-1]

		# ---FOR DEBUGGING:---print all headers and content-type header
		print('Headers:"', self.headers, '"')
		print('Content-Type:', self.headers['content-type'])
		# --------------------

		# grab length of body and then read the JSON body and load it into a dictionary
		length = int(self.headers['content-length'])
		body = self.rfile.read(length)
		dictionary = json.loads(body)

		# if the request was sent to '/login' -> someone is trying to log in. 
		if endpoint == "login":

			if loginUtils.verify_user_exists(self, dictionary['username']):
				if loginUtils.verify_password(loginUtils.return_users_password(self, dictionary['username']), dictionary['password']): 

					user_logged_in = loginUtils.get_user(self, dictionary['username'])
					session_id = sessionUtils.generate_session_id()
					session = sessionUtils.open_session(session_id, user_logged_in, '123.456.789.000')
					sessionUtils.add_session_to_db(session)
					auth_type = session.get_user_type()


					# create dictionary for success response and convert to JSON string and respond 200
					self.send_response(200)
					sess_cookie = http.cookies.SimpleCookie()
					auth_cookie = http.cookies.SimpleCookie()

					sess_cookie['user_session'] = session_id
					auth_cookie['auth_type'] = auth_type
					self.send_header('Access-Control-Allow-Headers', "Set-Cookie")
					self.send_header('Access-Control-Allow-Credentials', "true")
					self.send_header('Access-Control-Allow-Origin', "165.232.63.158")
					self.send_header("Set-Cookie", sess_cookie.output(header=''))
					self.send_header("Set-Cookie", auth_cookie.output(header=''))
					self.end_headers()
					res = {'success': 'true', 'sess_id': session_id, 'user_type': auth_type}
					json_res = json.dumps(res)
					bytes_str = json_res.encode('utf-8')
					self.wfile.write(bytes_str)
				else:
					# create dictionary for failure response and convert to JSON string and respond 404
					self.send_response(418)
					self.end_headers()
					res = {'success': 'false'}
					json_res = json.dumps(res)
					bytes_str = json_res.encode('utf-8')
					self.wfile.write(bytes_str)
			else:
				# create dictionary for failure response and convert to JSON string and respond 404
				self.send_response(418)
				self.end_headers()
				res = {'success': 'false'}
				json_res = json.dumps(res)
				bytes_str = json_res.encode('utf-8')
				self.wfile.write(bytes_str)
				
		# if the request was sent to '/register' -> someone is trying to register a new account. 
		elif endpoint == 'register':
			first_name = dictionary['firstName']
			last_name = dictionary['lastName']
			email = dictionary['email']
			username = dictionary['username']
			hashed_password = loginUtils.hash_password(dictionary['password'])

			user = User(None, first_name, last_name, username, hashed_password, email, 'basic')
			if (loginUtils.insert_new_user(self, user)):

				 # create dictionary for success response and convert to JSON string and respond 200
				self.send_response(200)
				self.end_headers()
				res = {'success': 'true'}
				json_res = json.dumps(res)
				bytes_str = json_res.encode('utf-8')
				self.wfile.write(bytes_str)
			else:
				 # create dictionary for failure response and convert to JSON string and respond 400
				self.send_response(400)
				self.end_headers()
				res = {'success': 'false'}
				json_res = json.dumps(res)
				bytes_str = json_res.encode('utf-8')
				self.wfile.write(bytes_str)
		elif endpoint == 'logout':
			
			sess_id = dictionary['sess_id']
			if loginUtils.logout_user(sess_id):

				sess_cookie = http.cookies.SimpleCookie()
				auth_cookie = http.cookies.SimpleCookie()


				self.send_response(200)
				
				sess_cookie['user_session'] = ""
				auth_cookie['auth_type'] = ""
				sess_cookie['user_session']['expires'] = datetime.utcnow()
				auth_cookie['auth_type']['expires'] = datetime.utcnow()
				

				self.send_header('Access-Control-Allow-Headers', "Set-Cookie")
				self.send_header('Access-Control-Allow-Credentials', "true")
				self.send_header('Access-Control-Allow-Origin', "165.232.63.158")
				

				self.send_header("Set-Cookie", sess_cookie.output(header=''))
				self.send_header("Set-Cookie", auth_cookie.output(header=''))

				self.end_headers()
			else:
				self.send_response(401)

		else:
			print("Else")


	# GET requests
	# will be completed when necessary, currently only using POST
	# def do_GET(self):
		# get path of request
		# path = self

def main():
	
	# SERVER PORT
	port = 4021
	
	# initialize the server using port and handler class as defined
	http_server = http.server.HTTPServer(('', port), SimpleHTTPRequestHandler)

	# ---FOR DEBUGGING:--- print message to confirm that the server is running successfully on defined port
	print("Running on port ", port)
	# --------------------

	http_server.serve_forever()


if __name__ == "__main__":
	main()
