import http.server
import http.cookies
from http.server import HTTPServer, BaseHTTPRequestHandler
import json
import urllib.parse
import requests
from sessionUtils import sessionUtils
from User import User
from Session import Session
import User

# Class Description: Acts in the backend of the Demand side server. Handles HTTP Requests sent to the server.
class authRequestHandler(BaseHTTPRequestHandler):

    # handle GET request
    def do_GET(self):

        endpoint_permissions = {"basic": ["apply", "dashboard", "download", "download_tmp"], 
                                "stato": ["apply", "dashboard", "download", "download_tmp", "upload"],
                                "admin": ["apply", "dashboard", "download", "download_tmp", "upload"]}

        requested_endpoint = self.headers['X-Original-URI'].strip("/")
        if "/" in requested_endpoint:
            requested_endpoint = requested_endpoint.split("/")[1]
        
        sess_id = self.headers['X-Sess-Id']
        print("SESS_ID:       ", sess_id)
        if not sess_id or sess_id is None:
            self.send_response(403)
            self.end_headers()
        
        sess_id = sess_id.strip()
        session = sessionUtils.get_session(sess_id)
        user_type = session.get_user_type()

        is_valid_request = requested_endpoint in endpoint_permissions[user_type]

        if session:
            if is_valid_request:
                self.send_response(200)
                self.end_headers()
            else:
                self.send_response(401)
                self.end_headers()
        else:
            self.send_response(403)
            self.end_headers()

def main():
    
    # SERVER PORT
    port = 4099
    
    # initialize the server using port and handler class as defined
    http_server = http.server.HTTPServer(('', port), authRequestHandler)

    # ---FOR DEBUGGING:--- print message to confirm that the server is running successfully on defined port
    print("Running on port ", port)
    # --------------------

    http_server.serve_forever()


if __name__ == "__main__":
    main()
