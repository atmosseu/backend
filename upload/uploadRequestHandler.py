import http.server
import re
import sys
import os, getpass
from http.server import HTTPServer, BaseHTTPRequestHandler
import json
import urllib.parse
import requests
import shutil

# Class Description: Acts in the backend of the Demand side server. Handles HTTP Requests sent to the server.
class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_POST(self):

        """Handle a POST request."""
        # Save files received in the POST
        wasSuccess, files_uploaded = self.handle_file_uploads()

        # Compose a response to the client
        response_obj = {
            "wasSuccess": wasSuccess,
            "files_uploaded": files_uploaded,
            "client_address": self.client_address
        }

        response_str = json.dumps(response_obj)

        self.log_message(response_str)

        # Send our response code, header, and data
        self.send_response(200)
        self.send_header("Content-type", "Application/json")
        self.send_header("Content-Length", len(response_str))
        self.end_headers()
        self.wfile.write(response_str.encode('utf-8'))

    def read_line_utf8(self):
        line_str = self.rfile.readline().decode('utf-8')
        self.char_remaining -= len(line_str)
        return line_str
    
    def read_line_audio(self):
        line_str = self.rfile.readline()
        self.char_remaining -= len(line_str)
        return line_str

    def handle_file_uploads(self):
        """
        Take the post request and save any files received to the same folder
        as this script.
        Returns
            wasSuccess: bool: whether the process was a success
            files_uploaded: list of string: files that were created
        """
        self.char_remaining = int(self.headers['content-length']) - 4
        # Find the boundary from content-type, which might look like:
        # 'multipart/form-data; boundary=----WebKitFormBoundaryUI1LY7c2BiEKGfFk'
        boundary = self.headers['content-type'].split("=")[1]

        # ----WebKitFormBoundaryUI1LY7c2BiEKGfFk
        line_str = self.read_line_utf8()
        if not boundary in line_str:
            self.log_message("Content did NOT begin with boundary as " +
                             "it should")
            return False, []
        # Content-Disposition: form-data; name="flightDate"
        line_str = self.read_line_utf8()
        # Blank line
        line_str = self.read_line_utf8()
        # Date from form 
        line_str = self.read_line_utf8()
        flight_date = line_str.rstrip('\r\n').split("-")
        flight_date_year = flight_date[0]
        flight_date_month = flight_date[1]
        flight_date_day = flight_date[2]

        

        # ----WebKitFormBoundaryUI1LY7c2BiEKGfFk
        line_str = self.read_line_utf8()
        # Content-Disposition: form-data; name="station"
        line_str = self.read_line_utf8()
        # Blank line
        line_str = self.read_line_utf8()
        # station from form 
        line_str = self.read_line_utf8()
        station_id = line_str.rstrip('\r\n')


        datepath = "".join([flight_date_year, flight_date_month, flight_date_day])
        path_to_add = [station_id, datepath, "pre"]

        basepath = os.path.join("/var","www","atmos","flights", "ozonesondes")

        if not os.path.exists(basepath):
            os.mkdir(basepath)
        for path in path_to_add:
            basepath = os.path.join(basepath, path)
            if not os.path.exists(basepath):
                os.mkdir(basepath)
        false_processed_upload(basepath)

        # ----WebKitFormBoundaryUI1LY7c2BiEKGfFk
        line_str = self.read_line_utf8()
        

        files_uploaded = []
        while self.char_remaining > 0:
            # Breaking out of this loop on anything except a boundary
            # an end-of-file will be a failure, so let's assume that
            wasSuccess = False

            # Content-Disposition: form-data; name="file"; filename="README.md"
            line_str = self.read_line_utf8()
            filename = re.findall('Content-Disposition.*name="file"; ' + 'filename="(.*)"', line_str)
            # Content-Type: application/octet-stream
            line_str = self.read_line_utf8()

            file_type = line_str.split(": ")[1]

            if not filename:
                self.log_message("Can't find filename " + filename)
                break
            else:
                filename = filename[0]
            filepath = os.path.join(basepath, filename)

                                              
            print("working with file:  ", filename)
            try:
                outfile = open(filepath, 'wb')
            except IOError:
                self.log_message("Can't create file " + str(filepath) +
                                " to write; do you have permission to write?")
                break

            # Blank line
            line_str = self.read_line_utf8()


            if "audio" in file_type:
                # First real line of code
                preline = self.read_line_audio()

                # Loop through the POST until we find another boundary line,
                # signifying the end of this file and the possible start of another
                while self.char_remaining > 0:
                    line_str = self.read_line_audio()

                    # ----WebKitFormBoundaryUI1LY7c2BiEKGfFk
                    if boundary in line_str:
                        preline = preline[0:-1]
                        if preline.endswith('\r'):
                            preline = preline[0:-1]
                        outfile.write(preline)
                        outfile.close()
                        self.log_message("File '%s' upload success!" % filename)
                        files_uploaded.append(filename)
                        # If this was the last file, the session was a success!
                        wasSuccess = True
                        break
                    else:
                        outfile.write(preline)
                        preline = line_str
            else:
                # First real line of code
                preline = self.read_line_utf8()

                # Loop through the POST until we find another boundary line,
                # signifying the end of this file and the possible start of another
                while self.char_remaining > 0:
                    line_str = self.read_line_utf8()

                    # ----WebKitFormBoundaryUI1LY7c2BiEKGfFk
                    if boundary in line_str:
                        preline = preline[0:-1]
                        if preline.endswith('\r'):
                            preline = preline[0:-1]
                        outfile.write(preline.encode('utf-8'))
                        outfile.close()
                        self.log_message("File '%s' upload success!" % filename)
                        files_uploaded.append(filename)
                        # If this was the last file, the session was a success!
                        wasSuccess = True
                        break
                    else:
                        outfile.write(preline.encode('utf-8'))
                        preline = line_str

        return wasSuccess, files_uploaded

def false_processed_upload(path_to_flight_pre_split):
        split_path, useless = os.path.split(path_to_flight_pre_split)
        false_processed_path_to_add = ["post", "history"]

        split_path = os.path.join(split_path, false_processed_path_to_add[0])
        os.mkdir(split_path)
        os.mkdir(os.path.join(split_path, false_processed_path_to_add[1]))

        processed_file_path = os.path.join("/home","atmos","backend","upload")
        processed_files = ["seac4rs-other_SONDES_2016090418_R1_L1.ict","nws_skewt_austin_20160904_1828.txt","austin_at005_20160904.kml","austin_2016090418_shadoz.dat","austin_2016090418_output.txt"]
        for filename in processed_files:
            with open(filename) as f:
                src = os.path.join(processed_file_path, filename)
                dst = os.path.join(split_path, filename)
                shutil.copy(src, dst)

def audio_upload(filename):
 print("audio upload!")

def main():
    
    # SERVER PORT
    port = 4182
    
    # initialize the server using port and handler class as defined
    http_server = http.server.HTTPServer(('', port), SimpleHTTPRequestHandler)

    # ---FOR DEBUGGING:--- print message to confirm that the server is running successfully on defined port
    print("Running on port ", port)
    # --------------------

    http_server.serve_forever()


if __name__ == "__main__":
    main()
