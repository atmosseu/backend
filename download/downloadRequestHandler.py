import http.server
import re
import sys
import os, getpass
from http.server import HTTPServer, BaseHTTPRequestHandler
import json
import urllib.parse
import requests
import wave
from mimetypes import guess_type

# Class Description: Acts in the backend of the Demand side server. Handles HTTP Requests sent to the server.
class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    

    def do_GET(self):

        extension_to_mimetype = {".wma": "audio/x-ms-wma", ".csv": "application/vnd.ms-excel", ".kml": "application/octet-stream", 
                            ".raw": "image/RAW", ".rawconfig": "application/octet-stream", ".pdf": "application/pdf", ".dat": "application/octet-stream", 
                            ".de1": "application/octet-stream"}

        length = int(self.headers['content-length'])
        body = self.rfile.read(length)
        dictionary = json.loads(body)

        file_path = dictionary['file_path'].split("/")
        filename = file_path[-1]
        file_extension = "." + filename.split(".")[1]
        actual_path = os.path.join("/" + file_path[0])
        for i in file_path[1:]:
            actual_path = os.path.join(actual_path, i)
        if file_extension in extension_to_mimetype.keys():
            mimetype = extension_to_mimetype[file_extension]
        else:
            mimetype, enc = guess_type(filename)
            if not mimetype:
                mimetype = "application/octet-stream"

        self.send_response(200)
        self.send_header('Content-Type', mimetype)
        self.send_header('Content-Disposition', 'name="file"; filename='+filename)
        self.end_headers()
        with open(actual_path, 'rb') as f:
            self.wfile.write(f.read())
        


def main():
    
    # SERVER PORT
    port = 4183
    
    # initialize the server using port and handler class as defined
    http_server = http.server.HTTPServer(('', port), SimpleHTTPRequestHandler)

    # ---FOR DEBUGGING:--- print message to confirm that the server is running successfully on defined port
    print("Running on port ", port)
    # --------------------

    http_server.serve_forever()


if __name__ == "__main__":
    main()
