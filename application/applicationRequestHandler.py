import http.server
import http.cookies
from http.server import HTTPServer, BaseHTTPRequestHandler
import json
import urllib.parse
import requests
from applicationUtils import applicationUtils
from loginUtils import loginUtils
from sessionUtils import sessionUtils
from User import User
from Station import Station
from datetime import datetime

# Class Description: Acts in the backend of the Demand side server. Handles HTTP Requests sent to the server.
class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    # handle POST request
    def do_POST(self):
        
        # get path of the request and then determine where the request was sent to
        path = self.path
        endpoint = path.split("/")[-1]

        # ---FOR DEBUGGING:---print all headers and content-type header
        print('Headers:"', self.headers, '"')
        print('Content-Type:', self.headers['content-type'])
        # --------------------

        # grab length of body and then read the JSON body and load it into a dictionary
        length = int(self.headers['content-length'])
        body = self.rfile.read(length)
        dictionary = json.loads(body)
        command = dictionary['command']
        # if the request was sent to '/login' -> someone is trying to log in. 
        if endpoint == "application":
            if command == 'submit':

                sess_id = dictionary['sess_id']
                session = sessionUtils.get_session(sess_id)
                if not session:
                    # create dictionary for failure response and convert to JSON string and respond 404
                    self.send_response(404)
                    self.end_headers()
                    res = {'success': 'false'}
                    json_res = json.dumps(res)
                    bytes_str = json_res.encode('utf-8')
                    self.wfile.write(bytes_str)

                user_id = session.user_id
                user = loginUtils.get_user_from_id(user_id)
                station = Station(None, dictionary['latitude'], dictionary['longitude'], dictionary['elevation'], dictionary['country'], dictionary['state'], dictionary['name'])
                if applicationUtils.add_new_application(user, station):

                    # create dictionary for success response and convert to JSON string and respond 200
                    self.send_response(200)
                    res = {'success': 'true'}
                    json_res = json.dumps(res)
                    bytes_str = json_res.encode('utf-8')
                    self.wfile.write(bytes_str)
                else:
                    # create dictionary for failure response and convert to JSON string and respond 404
                    self.send_response(404)
                    self.end_headers()
                    res = {'success': 'false'}
                    json_res = json.dumps(res)
                    bytes_str = json_res.encode('utf-8')
                    self.wfile.write(bytes_str)
            else:
                # create dictionary for failure response and convert to JSON string and respond 404
                self.send_response(404)
                self.end_headers()
                res = {'success': 'false'}
                json_res = json.dumps(res)
                bytes_str = json_res.encode('utf-8')
                self.wfile.write(bytes_str)
                
        # if the request was sent to '/register' -> someone is trying to register a new account. 
        elif endpoint == 'users':
            print("users")

        else:
            print("Else")


    # GET requests
    # will be completed when necessary, currently only using POST
    def do_GET(self):
        path = self.path
        endpoint_params = path.split("/")[-1].split("?")
        parameters = endpoint_params[1]
        
        endpoint = endpoint_params[0]
        

        # if the request was sent to '/list-vehicles' -> a list of vehicles and their information is being requested.
        if endpoint == 'list-applications':

            # if parameter given in path is 'all' -> list of ALL vehicles and their information is being requested
            if parameters == "all":

                # get all vehicle information from database
                apps = applicationUtils.get_all_applications()

                self.send_response(200)
                self.end_headers()
                res = {'success': 'true', 'applications': apps}
                json_res = json.dumps(res, default=datetime_handler)
                bytes_str = json_res.encode('utf-8')
                self.wfile.write(bytes_str)
            else:
                self.send_response(404)
                self.end_headers()
                res = {'success': 'false'}
                json_res = json.dumps(res)
                bytes_str = json_res.encode('utf-8')
                self.wfile.write(bytes_str)
        else:
            self.send_response(404)
            self.end_headers()
            res = {'success': 'false'}
            json_res = json.dumps(res)
            bytes_str = json_res.encode('utf-8')
            self.wfile.write(bytes_str)


def datetime_handler(obj):
    if isinstance(obj, datetime):
        return str(obj)
    raise TypeError("Unknown Type")
def main():
    
    # SERVER PORT
    port = 4007
    
    # initialize the server using port and handler class as defined
    http_server = http.server.HTTPServer(('', port), SimpleHTTPRequestHandler)

    # ---FOR DEBUGGING:--- print message to confirm that the server is running successfully on defined port
    print("Running on port ", port)
    # --------------------

    http_server.serve_forever()


if __name__ == "__main__":
    main()
