import pymysql.cursors
import datetime
import hashlib
import binascii
import os
from User import User
import json
from sessionUtils import sessionUtils
import Session


with open("sql_config.json") as db_config:
        DB_INFO = json.load(db_config)['mysql']
    
def get_connection():
    return pymysql.connect(host=DB_INFO['host'], port=DB_INFO['port'], user=DB_INFO['user'], password=DB_INFO['password'], db=DB_INFO['db'], charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)

# Utils class containing various helper methods to be used primarily by team21-cs-HTTPRequestHandler.py
class loginUtils:


    # Function Desc: Hashes a given password with salt using pbkdf2 hash alg and 16 byte salt
    # Pre: Parameter given is a String (password)
    # Post: Returns a String (hashed and salted representation of passed String)
    def hash_password(plain_password):
        salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
        hashed = hashlib.pbkdf2_hmac('sha512', plain_password.encode('utf-8'), salt, 100000)

        return (salt + binascii.hexlify(hashed)).decode('ascii')


    # Function Description: Validates a given plain password against a given hashed and salted password.
    #                       Takes salt from given hashed pass, and hashes/salts given plain pass with found salt.
    #                       Compares newly hashed/salted pass with pre given hashed/salted pass.
    # Pre: Parameters given are both Strings (hashed/salted pass and plain pass)
    # Post: Returns True if plain password matches hashed and salted pass, False if not.
    def verify_password(stored_password, password_to_validate):
        salt = stored_password[:64]
        to_compare = stored_password[64:]
        hashed = hashlib.pbkdf2_hmac('sha512', password_to_validate.encode('utf-8'), salt.encode('ascii'), 100000)
        hashed = binascii.hexlify(hashed).decode('ascii')

        return hashed == to_compare

    # Program Description: Verify user login
    # Pre: passed in variables are of type
    # post: During login the input username and password are matched to the database users, populates User upon successful log in
    def get_user(self, given_username):

        #Create a connection to Database
        connection = get_connection()

        with connection.cursor() as cursor:

            # Get user info from database
            sql = "SELECT user_id, firstname, lastname, email, user_type FROM users WHERE username = %s"

            cursor.execute(sql, given_username)
            result = cursor.fetchone()
            # close connection
            connection.close()
            user = User(result['user_id'], result['firstname'], result['lastname'], given_username, 'hash', result['email'], result['user_type'])
            return user


    def get_user_from_id(user_id):
        #Create a connection to Database
        connection = get_connection()

        with connection.cursor() as cursor:

            # Get user info from database
            sql = "SELECT username, firstname, lastname, email, user_type FROM users WHERE user_id = %s"

            cursor.execute(sql, user_id)
            result = cursor.fetchone()
            # close connection
            connection.close()
            user = User(user_id, result['firstname'], result['lastname'], result['username'], 'hash', result['email'], result['user_type'])
            return user


    def verify_user_exists(self, username):
        #Create a connection to Database
        connection = get_connection()

        with connection.cursor() as cursor:

            # Get user info from database
            sql = "SELECT * FROM users WHERE username = %s"

            cursor.execute(sql, username)
            result = cursor.fetchone()

            # close connection
            connection.close()
        if result:
            return True
        return False



    # Program Description: insert new user into demand database in the users table
    # Pre: The passed in variables are all strings
    # Post: The new User record is successfully created and inserted into the demand database Users table, then the connection is closed
    def insert_new_user(self, user):
        # connect to the database
        connection = get_connection()

        with connection.cursor() as cursor:
            # Insert user info into database
            sql = "INSERT INTO users (firstname, lastname, email, username, password) VALUES (%s, %s, %s, %s, %s)"

            cursor.execute(sql, [user.get_first_name(), user.get_last_name(), user.get_email(), user.get_username(), user.get_password()])

            # commit to save changes to database
            connection.commit()
            # close connection
            connection.close()
        return True

    def logout_user(sess_id):
        user_session = sessionUtils.get_session(sess_id)
        was_logged_out = False
        if user_session:
            was_logged_out = sessionUtils.kill_session(sess_id)
        return was_logged_out


    # Program Description: Function returns the password associated with the passed in username found in the demand databases User table
    # Pre: The passed in variable is a string
    # post: The function returns a string of the password that is pulled from the database table
    def return_users_password(self, given_username):
        # connect to the database
        connection = get_connection()
        
        with connection.cursor() as cursor:
            # read a single record
            sql = "SELECT password FROM users WHERE username= %s"
            cursor.execute(sql, given_username)
            # return password
            result = cursor.fetchone()
            # close connection
        connection.close()
        if result:
            return result['password']
        return False

        # Program Description: Function returns the id associated witht he passed in username found in the demand databases User table
        # Pre: The passed in value is a string
        # post: The function returns an int of the id that is pulled from the database table
    def return_users_id(self, user):
        # connect to the database
        connection = get_connection()

        with connection.cursor() as cursor:
            # read a single record
            sql = "SELECT user_id FROM users WHERE username= %s"
            cursor.execute(sql, user.get_username())
            # return password
            result = cursor.fetchone()

            # close connection
            connection.close()
            return result['user_id']




if __name__ == "__main__":
    main()
