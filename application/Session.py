# User object class
class Session:

    # Constructor/Initializer
    def __init__(self, user_id, sess_id, expires, user_type, user_ip, last_activity):
        self.sess_id = sess_id
        self.user_id = user_id
        self.expires = expires
        self.user_type = user_type
        self.user_ip = user_ip
        self.last_activity = last_activity

    # Desc: Function to get the User object's user_id attribute
    # Pre: Object must have its user_id attribute set
    # Post: Returns the value of the object's user_id attribute
    def get_user_id(self):
        return self.user_id

    def get_last_activity(self):
        return self.last_activity


    # Desc: Function to get the User object's first_name attribute
    # Pre: Object must have its first_name attribute set
    # Post: Returns the value of the object's first_name attribute
    def get_sess_id(self):
        return self.sess_id


    # Desc: Function to get the User object's last_name attribute
    # Pre: Object must have its last_name attribute set
    # Post: Returns the value of the object's last_name attribute
    def get_expires(self):
        return self.expires


    # Desc: Function to get the User object's username attribute
    # Pre: Object must have its username attribute set
    # Post: Returns the value of the object's username attribute
    def get_user_type(self):
        return self.user_type


    # Desc: Function to get the User object's password attribute
    # Pre: Object must have its password attribute set
    # Post: Returns the value of the object's password attribute
    def get_user_ip(self):
        return self.user_ip


    # Desc: Function to set the User object's user_id attribute
    # Pre: Object must exist (must be already initialized), parameter given should be Int
    # Post: Value of the User object's user_id attribute is set to the given parameter
    def set_user_id(self, user_id):
        self.user_id = user_id


    # Desc: Function to set the User object's first_name attribute
    # Pre: Object must exist (must be already initialized), parameter given should be String
    # Post: Value of the User object's first_name attribute is set to the given parameter
    def set_sess_id(self, sess_id):
        self.sess_id = sess_id


    # Desc: Function to set the User object's last_name attribute
    # Pre: Object must exist (must be already initialized), parameter given should be String
    # Post: Value of the User object's last_name attribute is set to the given parameter
    def set_expires(self, expires):
        self.expires = expires


    # Desc: Function to set the User object's username attribute
    # Pre: Object must exist (must be already initialized), parameter given should be String
    # Post: Value of the User object's username attribute is set to the given parameter
    def set_user_type(self, user_type):
        self.user_type = user_type


    # Desc: Function to set the User object's password attribute
    # Pre: Object must exist (must be already initialized), parameter given should be String
    # Post: Value of the User object's password attribute is set to the given parameter
    def set_user_ip(self, user_ip):
        self.user_ip = user_ip


    def set_last_activity(self, activity):
        self.last_activity = activity