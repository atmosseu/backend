# User object class
class User:

    # Constructor/Initializer
    def __init__(self, user_id, first_name, last_name, username, password, email, user_type):
        self.user_id = user_id
        self.first_name = first_name
        self.last_name = last_name
        self.username = username
        self.password = password
        self.email = email
        self.user_type = user_type

    # Desc: Function to get the User object's user_id attribute
    # Pre: Object must have its user_id attribute set
    # Post: Returns the value of the object's user_id attribute
    def get_user_id(self):
        return self.user_id

    def get_user_type(self):
        return self.user_type

    # Desc: Function to get the User object's first_name attribute
    # Pre: Object must have its first_name attribute set
    # Post: Returns the value of the object's first_name attribute
    def get_first_name(self):
        return self.first_name


    # Desc: Function to get the User object's last_name attribute
    # Pre: Object must have its last_name attribute set
    # Post: Returns the value of the object's last_name attribute
    def get_last_name(self):
        return self.last_name


    # Desc: Function to get the User object's username attribute
    # Pre: Object must have its username attribute set
    # Post: Returns the value of the object's username attribute
    def get_username(self):
        return self.username


    # Desc: Function to get the User object's password attribute
    # Pre: Object must have its password attribute set
    # Post: Returns the value of the object's password attribute
    def get_password(self):
        return self.password


    # Desc: Function to get the User object's email attribute
    # Pre: Object must have its email attribute set
    # Post: Returns the value of the object's email attribute
    def get_email(self):
        return self.email


    # Desc: Function to set the User object's user_id attribute
    # Pre: Object must exist (must be already initialized), parameter given should be Int
    # Post: Value of the User object's user_id attribute is set to the given parameter
    def set_user_id(self, user_id):
        self.user_id = user_id

    def set_user_type(self, user_type):
        self.user_type = user_type

    # Desc: Function to set the User object's first_name attribute
    # Pre: Object must exist (must be already initialized), parameter given should be String
    # Post: Value of the User object's first_name attribute is set to the given parameter
    def set_first_name(self, first_name):
        self.first_name = first_name


    # Desc: Function to set the User object's last_name attribute
    # Pre: Object must exist (must be already initialized), parameter given should be String
    # Post: Value of the User object's last_name attribute is set to the given parameter
    def set_last_name(self, last_name):
        self.last_name = last_name


    # Desc: Function to set the User object's username attribute
    # Pre: Object must exist (must be already initialized), parameter given should be String
    # Post: Value of the User object's username attribute is set to the given parameter
    def set_username(self, username):
        self.username = username


    # Desc: Function to set the User object's password attribute
    # Pre: Object must exist (must be already initialized), parameter given should be String
    # Post: Value of the User object's password attribute is set to the given parameter
    def set_password(self, password):
        self.password = password


    # Desc: Function to set the User object's email attribute
    # Pre: Object must exist (must be already initialized), parameter given should be String
    # Post: Value of the User object's email attribute is set to the given parameter
    def set_email(self, email):
        self.email = email
