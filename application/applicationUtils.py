import pymysql.cursors
from datetime import datetime
import binascii
import os
from User import User
from Station import Station
import json


with open("sql_config.json") as db_config:
        DB_INFO = json.load(db_config)['mysql']
    
def get_connection():
    return pymysql.connect(host=DB_INFO['host'], port=DB_INFO['port'], user=DB_INFO['user'], password=DB_INFO['password'], db=DB_INFO['db'], charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)

# Utils class containing various helper methods to be used primarily by team21-cs-HTTPRequestHandler.py
class applicationUtils:


    def add_new_application(user, station):
        connection = get_connection()
        
        current_time = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        user_id = user.user_id
        user_email = user.email
        lat = station.latitude
        lon = station.longitude
        ele = station.elevation
        country = station.country
        state = station.state
        name = station.name
        with connection.cursor() as cursor:
            sql = "INSERT INTO applications (app_submitted, user_id, station_latitude, station_longitude, station_elevation, station_country, station_state, station_name, user_email) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
            cursor.execute(sql, [current_time, user_id, lat, lon, ele, country, state, name, user_email]) 
            connection.commit()
        connection.close()
        return True

    def decide_application(app_id, decision):
        connection = get_connection()
        
        current_time = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        with connection.cursor() as cursor:
            sql = "ALTER TABLE applications set app_decision = %s, app_closed = %s WHERE app_id = %s"
            cursor.execute(sql, [decision, current_time, app_id]) 
            connection.commit()
        connection.close()
        return True

    def get_all_applications():
        connection = get_connection()
        
        with connection.cursor() as cursor:
            sql = "SELECT * FROM applications where app_decision is Null"
            cursor.execute(sql)
            result = cursor.fetchall()
            connection.commit()

            # close connection
            connection.close()
            return result



if __name__ == "__main__":
    main()
