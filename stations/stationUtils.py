import pymysql.cursors
import datetime
import hashlib
import binascii
import os
from User import User
from Station import Station
import json


with open("sql_config.json") as db_config:
        DB_INFO = json.load(db_config)['mysql']
    
def get_connection():
    return pymysql.connect(host=DB_INFO['host'], port=DB_INFO['port'], user=DB_INFO['user'], password=DB_INFO['password'], db=DB_INFO['db'], charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)

# Utils class containing various helper methods to be used primarily by team21-cs-HTTPRequestHandler.py
class stationUtils:

    def does_station_exist(station):
        connection = get_connection()

        does_station_exist = False
        with connection.cursor() as cursor:
            sql = "SELECT station_id from stations where latitude=%s, longitude=%s"
            cursor.execute(sql, [station.latitude, station.longitude]) 
            result = cursor.fetchone()
            if result:
                does_station_exist = True
            connection.commit()
        connection.close()
        return does_station_exist

    def add_new_station(station):
        connection = get_connection()

        with connection.cursor() as cursor:
            sql = "INSERT INTO stations (latitude, longitude, elevation, country, state, name) VALUES (%s, %s, %s, %s, %s, %s)"
            cursor.execute(sql, [station.latitude, station.longitude, station.elevation, station.country, station.state, station.name]) 
            connection.commit()
        connection.close()
        return True

    def update_station(station):
        connection = get_connection()

        with connection.cursor() as cursor:
            sql = "UPDATE stations SET latitude=%s, longitude=%s, elevation=%s, country=%s, state=%s, name=%s WHERE station_id=%s"
            cursor.execute(sql, [station.latitude, station.longitude, station.elevation, station.country, station.state, station.name, station.station_id]) 
            connection.commit()
        connection.close()
        return True


if __name__ == "__main__":
    main()
