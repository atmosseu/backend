import http.server
import http.cookies
from http.server import HTTPServer, BaseHTTPRequestHandler
import json
import urllib.parse
import requests
from stationUtils import stationUtils
from User import User
from Station import Station
from datetime import datetime

# Class Description: Acts in the backend of the Demand side server. Handles HTTP Requests sent to the server.
class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    # handle POST request
    def do_POST(self):
        
        # get path of the request and then determine where the request was sent to
        path = self.path
        endpoint = path.split("/")[-1]

        # ---FOR DEBUGGING:---print all headers and content-type header
        print('Headers:"', self.headers, '"')
        print('Content-Type:', self.headers['content-type'])
        # --------------------

        # grab length of body and then read the JSON body and load it into a dictionary
        length = int(self.headers['content-length'])
        body = self.rfile.read(length)
        dictionary = json.loads(body)
        command = dictionary['command']
        # if the request was sent to '/login' -> someone is trying to log in. 
        print("Else")


    # GET requests
    # will be completed when necessary, currently only using POST
    def do_GET(self):
        path = self.path
        endpoint_params = path.split("/")[-1].split("?")
        parameters = endpoint_params[1]
        
        endpoint = endpoint_params[0]
        

        # if the request was sent to '/list-vehicles' -> a list of vehicles and their information is being requested.
        if endpoint == 'list-stations':

            # if parameter given in path is 'all' -> list of ALL vehicles and their information is being requested
            if parameters == "all":

                # get all vehicle information from database
                stations = stationUtils.get_all_stations()
                
         
                if stations:
                    self.send_response(200)
                    self.end_headers()
                    res = {'success': 'true', 'stations': stations}
                    json_res = json.dumps(res)
                    bytes_str = json_res.encode('utf-8')
                    self.wfile.write(bytes_str)
                else:
                    self.send_response(404)
                    self.end_headers()
                    res = {'success': 'false'}
                    json_res = json.dumps(res)
                    bytes_str = json_res.encode('utf-8')
                    self.wfile.write(bytes_str)
            else:
                self.send_response(404)
                self.end_headers()
                res = {'success': 'false'}
                json_res = json.dumps(res)
                bytes_str = json_res.encode('utf-8')
                self.wfile.write(bytes_str)
        else:
            self.send_response(404)
            self.end_headers()
            res = {'success': 'false'}
            json_res = json.dumps(res)
            bytes_str = json_res.encode('utf-8')
            self.wfile.write(bytes_str)
def main():
    
    # SERVER PORT
    port = 4034
    
    # initialize the server using port and handler class as defined
    http_server = http.server.HTTPServer(('', port), SimpleHTTPRequestHandler)

    # ---FOR DEBUGGING:--- print message to confirm that the server is running successfully on defined port
    print("Running on port ", port)
    # --------------------

    http_server.serve_forever()


if __name__ == "__main__":
    main()
