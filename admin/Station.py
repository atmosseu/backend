# User object class
class Station:

    # Constructor/Initializer
    def __init__(self, station_id, latitude, longitude, elevation, country, state, name):
        self.station_id = station_id
        self.latitude = latitude
        self.longitude = longitude
        self.elevation = elevation
        self.country = country
        self.state = state
        self.name = name

    # Desc: Function to get the User object's user_id attribute
    # Pre: Object must have its user_id attribute set
    # Post: Returns the value of the object's user_id attribute
    def get_station_id(self):
        return self.station_id

    def get_name(self):
        return self.name

    # Desc: Function to get the User object's latitude attribute
    # Pre: Object must have its latitude attribute set
    # Post: Returns the value of the object's latitude attribute
    def get_latitude(self):
        return self.latitude


    # Desc: Function to get the User object's longitude attribute
    # Pre: Object must have its longitude attribute set
    # Post: Returns the value of the object's longitude attribute
    def get_longitude(self):
        return self.longitude


    # Desc: Function to get the User object's elevation attribute
    # Pre: Object must have its elevation attribute set
    # Post: Returns the value of the object's elevation attribute
    def get_elevation(self):
        return self.elevation


    # Desc: Function to get the User object's country attribute
    # Pre: Object must have its country attribute set
    # Post: Returns the value of the object's country attribute
    def get_country(self):
        return self.country


    # Desc: Function to get the User object's state attribute
    # Pre: Object must have its state attribute set
    # Post: Returns the value of the object's state attribute
    def get_state(self):
        return self.state


    # Desc: Function to set the User object's user_id attribute
    # Pre: Object must exist (must be already initialized), parameter given should be Int
    # Post: Value of the User object's user_id attribute is set to the given parameter
    def set_station_id(self, station_id):
        self.station_id = station_id

    def set_name(self, name):
        self.name = name

    # Desc: Function to set the User object's latitude attribute
    # Pre: Object must exist (must be already initialized), parameter given should be String
    # Post: Value of the User object's latitude attribute is set to the given parameter
    def set_latitude(self, latitude):
        self.latitude = latitude


    # Desc: Function to set the User object's longitude attribute
    # Pre: Object must exist (must be already initialized), parameter given should be String
    # Post: Value of the User object's longitude attribute is set to the given parameter
    def set_longitude(self, longitude):
        self.longitude = longitude


    # Desc: Function to set the User object's elevation attribute
    # Pre: Object must exist (must be already initialized), parameter given should be String
    # Post: Value of the User object's elevation attribute is set to the given parameter
    def set_elevation(self, elevation):
        self.elevation = elevation


    # Desc: Function to set the User object's country attribute
    # Pre: Object must exist (must be already initialized), parameter given should be String
    # Post: Value of the User object's country attribute is set to the given parameter
    def set_country(self, country):
        self.country = country


    # Desc: Function to set the User object's state attribute
    # Pre: Object must exist (must be already initialized), parameter given should be String
    # Post: Value of the User object's state attribute is set to the given parameter
    def set_state(self, state):
        self.state = state
