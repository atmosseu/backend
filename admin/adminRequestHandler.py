import http.server
import http.cookies
from http.server import HTTPServer, BaseHTTPRequestHandler
import json
import urllib.parse
import requests
from stationUtils import stationUtils
from loginUtils import loginUtils
from applicationUtils import applicationUtils
from User import User
from Station import Station
from datetime import datetime

# Class Description: Acts in the backend of the Demand side server. Handles HTTP Requests sent to the server.
class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

	# handle POST request
	def do_POST(self):
		
		# get path of the request and then determine where the request was sent to
		path = self.path
		endpoint = path.split("/")[-1]

		# ---FOR DEBUGGING:---print all headers and content-type header
		print('Headers:"', self.headers, '"')
		print('Content-Type:', self.headers['content-type'])
		# --------------------

		# grab length of body and then read the JSON body and load it into a dictionary
		length = int(self.headers['content-length'])
		body = self.rfile.read(length)
		dictionary = json.loads(body)
		command = dictionary['command']
		# if the request was sent to '/login' -> someone is trying to log in. 
		if endpoint == "station":
			if command == 'add':

				station = Station(None, dictionary['latitude'], dictionary['longitude'], dictionary['elevation'], dictionary['country'], dictionary['state'], dictionary['name'])
				if adminUtils.add_new_station(station):

					# create dictionary for success response and convert to JSON string and respond 200
					self.send_response(200)
				
					res = {'success': 'true'}
					json_res = json.dumps(res)
					bytes_str = json_res.encode('utf-8')
					self.wfile.write(bytes_str)
				else:
					# create dictionary for failure response and convert to JSON string and respond 404
					self.send_response(404)
					self.end_headers()
					res = {'success': 'false'}
					json_res = json.dumps(res)
					bytes_str = json_res.encode('utf-8')
					self.wfile.write(bytes_str)
			elif command == 'alter':
				station = Station(dictionary['station_id'], dictionary['latitude'], dictionary['longitude'], dictionary['elevation'], dictionary['country'], dictionary['state'], dictionary['name'])
				if adminUtils.update_station(station):

					# create dictionary for success response and convert to JSON string and respond 200
					self.send_response(200)
				
					res = {'success': 'true'}
					json_res = json.dumps(res)
					bytes_str = json_res.encode('utf-8')
					self.wfile.write(bytes_str)
				else:
					# create dictionary for failure response and convert to JSON string and respond 404
					self.send_response(404)
					self.end_headers()
					res = {'success': 'false'}
					json_res = json.dumps(res)
					bytes_str = json_res.encode('utf-8')
					self.wfile.write(bytes_str)
			elif command == 'remove':
				if adminUtils.remove_station(dictionary['station_id']):
					# create dictionary for success response and convert to JSON string and respond 200
					self.send_response(200)
				
					res = {'success': 'true'}
					json_res = json.dumps(res)
					bytes_str = json_res.encode('utf-8')
					self.wfile.write(bytes_str)
				else:
					# create dictionary for failure response and convert to JSON string and respond 404
					self.send_response(404)
					self.end_headers()
					res = {'success': 'false'}
					json_res = json.dumps(res)
					bytes_str = json_res.encode('utf-8')
					self.wfile.write(bytes_str)
			else:
				# create dictionary for failure response and convert to JSON string and respond 404
				self.send_response(404)
				self.end_headers()
				res = {'success': 'false'}
				json_res = json.dumps(res)
				bytes_str = json_res.encode('utf-8')
				self.wfile.write(bytes_str)
				
		# if the request was sent to '/register' -> someone is trying to register a new account. 
		elif endpoint == 'application':
			if command == 'decide':
				app_id = dictionary['app_id']
				if applicationUtils.decide_application(app_id, dictionary['decision']):
					station = applicationUtils.get_app_station(app_id)
					stationUtils.add_new_station(station)
					# create dictionary for success response and convert to JSON string and respond 200
					self.send_response(200)
					res = {'success': 'true'}
					json_res = json.dumps(res)
					bytes_str = json_res.encode('utf-8')
					self.wfile.write(bytes_str)
				else:
					# create dictionary for failure response and convert to JSON string and respond 404
					self.send_response(404)
					self.end_headers()
					res = {'success': 'false'}
					json_res = json.dumps(res)
					bytes_str = json_res.encode('utf-8')
					self.wfile.write(bytes_str)
			else:
				# create dictionary for failure response and convert to JSON string and respond 404
				self.send_response(404)
				self.end_headers()
				res = {'success': 'false'}
				json_res = json.dumps(res)
				bytes_str = json_res.encode('utf-8')
				self.wfile.write(bytes_str)


		else:
			print("Else")


	# GET requests
	# will be completed when necessary, currently only using POST
	# def do_GET(self):
		# get path of request
		# path = self

def main():
	
	# SERVER PORT
	port = 4040
	
	# initialize the server using port and handler class as defined
	http_server = http.server.HTTPServer(('', port), SimpleHTTPRequestHandler)

	# ---FOR DEBUGGING:--- print message to confirm that the server is running successfully on defined port
	print("Running on port ", port)
	# --------------------

	http_server.serve_forever()


if __name__ == "__main__":
	main()
